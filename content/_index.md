---
title: "Home"
date: 2021-09-18T23:27:57-03:00
draft: false
image: /images/home2.png
---
## Home

Este é um site com os conteúdos necessários para você ingressar no universo de League Of Legends.

Neste site, você encontrará informações básicas sobre:

- Campeões;
- Itens;
- Mapa e rotas;

Para mais informaçõe sobre o jogo, acesse o [site oficial](https://www.leagueoflegends.com/pt-br/).