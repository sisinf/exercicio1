---
title: "Mapa e rotas"
date: 2021-09-18T23:27:57-03:00
draft: false
---

O jogo de League Of Legends se passa em Summoner's Rift e possui três rotas principais rodeadas pela *jungle*:

![alt-text](../mapa.jpg)

O jogo é disputado por dois times, cada qual com 5 campeões. Os campeões iniciam o jogo em um dos *Nexus* e em seguida se distribuem da seguinte forma:

- Um campeão de cada time na rota superior;
- Um campeão de cada time na rota do meio;
- Dois campeões de cada time na rota inferior;
- Um campeão de cada time na *jungle*;

O território de cada time é inicialmente dividido pelo rio. Os campeões em cada rota ficam a coletar experiência e dinheiro dos minions, buscando avançar em sua rota por meio da destruição das torres. O time vencedor é aquele que destruir primeiro o *Nexus* inimigo.