---
title: "Itens"
date: 2021-10-21T18:11:08-03:00
draft: false
---

Cada campeão é capaz de, ao longo do jogo, comprar itens para aprimorar suas propriedades de dano, defesa e saúde. Abaixo apresentamos alguns deles.

![alt-text](../itens.jpg)

Para efetuar a compra destes itens, o campeão deve estar próximo à loja e possuir a quantia de dinheiro requerida.  
Cada item possui características próprias com ativos e passivos que podem ser lidos na loja a qualquer momento do jogo.