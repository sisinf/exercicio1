---
title: "Campeões"
date: 2021-10-21T18:11:08-03:00
draft: false
---

O League of Legends conta com mais de 100 campeões, de todas as formas, tamanhos e cores:

![alt-text](../champ.jpg)

Estes campeões possuem habilidades, histórias e interações com o mapa únicas e diversas, de forma que a jogabilidade nunca é a mesma e permite que o jogador tenha diferentes experiências a cada jogo.

---

### Listagem dos campeões:

1. Aatrox
2. Ahri
3. Akali
4. Alistar
5. Amumu
6. Anivia
7. Annie
8. Ashe
9. Azir
10. Bard
11. Blitzcrank
12. Brand
13. Braum
14. Caitlyn 
15. Cassiopeia
16. Cho'Gath
17. Corki
18. Darius
19. Diana
20. Dr. Mundo
21. Draven
22. Ekko
23. Elise
24. Evelynn
25. Ezreal 
26. Fiddlesticks
27. Fiora
28. Fizz
29. Galio
30. Gangplank
31. Garen
32. Gnar
33. Gragas
34. Graves
35. Hecarim
36. Heimerdinger
37. Irelia
38. Janna
39. Jarvan IV
40. Jax
41. Jayce
42. Jinx
43. Kalista
44. Karma
45. Karthus
46. Kassadin
47. Katarina
48. Kayle
49. Kennen
50. Kha'Zix
51. Kindred
52. Kog'Maw
53. LeBlanc
54. Lee Sin
55. Leona
56. Lissandra
57. Lucian
58. Lulu
59. Lux
60. Malphite
61. Malzahar
62. Maokai
63. Master Yi
64. Miss Fortune
65. Mordekaiser
66. Morgana
67. Nami
68. Nasus
69. Nautilus
70. Nidalee
71. Nocturne
72. Nunu
73. Olaf
74. Orianna
75. Pantheon
76. Poppy
77. Quinn
78. Rammus
79. Rek'Sai
80. Renekton
90. Rengar
91. Riven
92. Rumble
93. Ryze
94. Sejuani
95. Shaco
96. Shen
97. Shyvana
98. Singed
99. Sion
100. Sivir
101. Skarner
102. Sona
103. Soraka
104. Swain
105. Syndra
106. Tahm Kench
107. Talon
108. Taric
109. Teemo
110. Thresh
111. Tristana
112. Trundle
113. Tryndamere
114. Twisted Fate
115. Twitch
116. Udyr
117. Urgot
118. Varus
119. Vayne
120. Veigar
121. Vel'Koz
122. Vi
123. Viktor
124. Vladimir
125. Volibear
126. Warwick
127. Wukong
128. Xerath
129. Xin Zhao
130. Yasuo
131. Yorick
132. Zac
133. Zed
134. Ziggs
135. Zilean
136. Zyra